package p;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import b.UserB;
import da.ConnectionUtil;
import net.miginfocom.swing.MigLayout;


public class LoginDialog extends JDialog{
	private final JPanel contentPane = new JPanel();;
	private JTextField txtUsername;
	private JPasswordField txtPassword;
	//private BossFrame boss;
//	private EmployeeFrame ems;
	private JLabel lblMessage;
	
	
	public LoginDialog(FirstFrame parent) {
//		this.first = first;
		
		super(parent, "Login", true);
		setAlwaysOnTop(true);
		setBounds(100, 100, 450, 300);
		
	//	this.boss = boss;
//		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][grow]", "[][][][]"));
		
		JLabel lblUsername = new JLabel("Username");
		contentPane.add(lblUsername, "cell 0 0,alignx trailing");
		
		txtUsername = new JTextField();
		contentPane.add(txtUsername, "cell 1 0,growx");
		txtUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		contentPane.add(lblPassword, "cell 0 1,alignx trailing");
		
		txtPassword = new JPasswordField();
		contentPane.add(txtPassword, "cell 1 1,growx");
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ConnectionUtil connect = new ConnectionUtil();
					UserB userB = new UserB();
					if(userB.checkLogin(txtUsername.getText(), txtPassword.getText())){
//						boss.enableControl();
						System.out.println("Login");
						LoginDialog.this.setVisible(false);
						
//						String sql = "SELECT role FROM user";
//						Connection conn = (Connection) connect.getConnection();
//						PreparedStatement stmt = (PreparedStatement) conn.prepareStatement(sql);
//						ResultSet rs = stmt.executeQuery();
//						if(rs.getInt(1) == 0 ) {
//							BossFrame bossFrame = new BossFrame(parent);
//							bossFrame.setVisible(true);
//							
//							parent.setVisible(false);
//						} else {
//							EmployeeFrame employeeframe = new EmployeeFrame();
//							employeeframe.setVisible(true);
//						}
								
						BossFrame bossFrame = new BossFrame(parent);
						bossFrame.setVisible(true);
						
						parent.setVisible(false);
						//boss.setVisible(true);
					}else{
						lblMessage.setForeground(Color.RED);
						lblMessage.setText("Incorect username or password!");
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		lblMessage = new JLabel("Please enter your username and password");
		contentPane.add(lblMessage, "cell 1 2");
		contentPane.add(btnLogin, "flowx,cell 1 3");
		
		JButton btnCancel = new JButton("Cancel");
		
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				LoginDialog.this.setVisible(false);
			}
		});
		contentPane.add(btnCancel, "cell 1 3");
	}
	
}
