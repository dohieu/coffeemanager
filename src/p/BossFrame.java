package p;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import b.UserB;


public class BossFrame extends JFrame {
	private JPanel contentPane;
	private JTable table;
	private DefaultTableModel model;
	private UserB user;
	private static BossFrame boss;
	private FirstFrame first;
//	private CheckInFrame checkIn;

//	public void setCheckInFrame(CheckInFrame checkIn) {
//		this.checkIn = checkIn;
//	}
//	
//	public void setBossFrame(BossFrame boss) {
//		this.boss = boss;
//	}
//	public static BossFrame getBossFrame() {
//		return boss;
//	}
//	
	public void setFirsFrame (FirstFrame first) {
		this.first = first;
	}
	
	public FirstFrame getFirstFrame (FirstFrame first) {
		return first;
	}
	private void initModelUser() throws SQLException {
		model = user.getAllUser();
		table.setModel(model);
	}


	public BossFrame(FirstFrame firstframe) {
		
		user = new UserB();
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(900, 600);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnUser = new JMenu("User");
		menuBar.add(mnUser);

		JMenuItem mntmLogOut = new JMenuItem("Log out");
		mnUser.add(mntmLogOut);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		scrollPane.setViewportView(table);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);


		mntmLogOut.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				BossFrame.this.setVisible(false);
				firstframe.setVisible(true);

			}
		});

		
	}

//	void enableControl(){
//		btnAdd.setEnabled(true);
//		btnDelete.setEnabled(true);
//		btnEdit.setEnabled(true);
//	}
}